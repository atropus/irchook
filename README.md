irchook
========

A simple to use git irchook using netcat

To enable it, put the script in .git/hooks directory and rename it as post-commit
Everytime you commit to that repo, netcat will send an irc message containing your commit information
Increase the netcat timeout if you are on a slower network, else commit message will never show up
